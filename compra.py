'''
Programa para mostrar la lista de la compra
'''

habitual = ('pan', 'leche', 'lentejas')
def main():
    especifica = []

    while True:
        elemento = input("Escribe los elementos que quieres añadir a la lista de la compra: ")
        if elemento == "":
            break
        especifica.append(elemento)

    compra_total = list(habitual) + especifica
    compra_total = list(set(compra_total))

    print("Lista de la compra: ")
    for elemento in compra_total:
        print(elemento)

    elementos_habituales = len(habitual)
    elementos_especificos = len(especifica)
    elementos_totales = len(compra_total)

    print("Elementos habituales: ", elementos_habituales)
    print("Elementos específicos: ", elementos_especificos)
    print("Elementos totales: ", elementos_totales)

if __name__ == '__main__':
    main()
